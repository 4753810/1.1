#!/bin/sh
while ! nc -z db 5432; do
      sleep 0.1
done
echo "PostgreSQL started"

python manage.py migrate
gunicorn Blog.wsgi:application --bind 0.0.0.0:8000
echo "Blog started"