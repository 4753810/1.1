FROM python:3.8.3
RUN useradd -d /home/django django
RUN ls -la /home; cat /etc/passwd;
ENV HOME=/home/django
ENV APP_HOME=/home/django/web
RUN mkdir -p $APP_HOME
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
RUN pip install --upgrade pip
COPY django_blog/requirements.txt .
RUN apt update; apt install -y gcc netcat python3-dev;  pip install -r requirements.txt
COPY django_blog .
RUN chown -R django:django $HOME

USER django

# run entrypoint.prod.sh
ENTRYPOINT ["entrypoint.sh"]


